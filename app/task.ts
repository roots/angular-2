export class Task {
  id: number;
  weight: number;
  title: string;
  description: string;
  complete: boolean;
  priority: number;
}