import { Component, OnInit } from '@angular/core';

import { Task } from './task';

import { TaskService } from './task.service';
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'my-tasks',
  templateUrl: 'tasks.component.html',
  providers: [TaskService],
})


export class TasksComponent implements OnInit {
 
  title = 'Angular 2 Do Detail';
  tasks: Task[];
  selectedTask: Task;
  
  constructor(private taskService: TaskService, private router: Router) { }
  
  getTasks(): void {
    this.taskService.getTasks().then(tasks => this.tasks = tasks);
  }

  ngOnInit(): void {
    this.getTasks();
  }
  
  onSelect(task: Task): void {
    this.selectedTask = task;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedTask.id]);
  }
}
