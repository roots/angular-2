import {Component} from "@angular/core";

@Component({
    selector: 'my-app',
    template: `
        <h1>{{title}}</h1>
        <nav>
            <a routerLink="/top-tasks" routerLinkActive="active">Top Tasks</a>
            <a routerLink="/tasks" routerLinkActive="active">All Tasks</a>
        </nav>
        <router-outlet></router-outlet>
      `
})

export class AppComponent{
    title = "Angular 2 Do";
}