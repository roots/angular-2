import {Component, OnInit} from "@angular/core";

import { Router } from '@angular/router';

import { Task } from './task';

import { TaskService } from './task.service';

@Component ({
    selector: 'top-tasks',
    moduleId: module.id,
    templateUrl: 'top-tasks.component.html',
    styleUrls: [ 'top-tasks.component.css' ]
})
export class TopTasksComponent implements OnInit {

    tasks: Task[] = [];

    constructor(
        private taskService: TaskService,
        private router: Router)
    { }

    ngOnInit(): void {
        this.taskService.getTasks()
            .then(tasks => this.tasks = tasks.slice(1, 5))
    }

    gotoDetail(task: Task): void {
        let link = ['/detail', task.id];
        this.router.navigate(link);
    }
}