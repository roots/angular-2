import { Task } from './task';

export const TASKS: Task[] = [
  { id: 1, weight: 11, title: 'task 1', description: 'Mr. Nice', complete: false, priority: 0 },
  { id: 2, weight: 12, title: 'task 2', description: 'Narco', complete: false, priority: 0 },
  { id: 3, weight: 13, title: 'task 3', description: 'Bombasto', complete: false, priority: 0 },
  { id: 4, weight: 14, title: 'task 4', description: 'Celeritas', complete: false, priority: 0 },
  { id: 5, weight: 15, title: 'task 5', description: 'Magneta', complete: false, priority: 0 },
  { id: 6, weight: 16, title: 'task 6', description: 'RubberMan', complete: false, priority: 0 },
  { id: 7, weight: 17, title: 'task 7', description: 'Dynama', complete: false, priority: 0 },
  { id: 8, weight: 18, title: 'task 8', description: 'Dr IQ', complete: false, priority: 0 },
  { id: 9, weight: 19, title: 'task 9', description: 'Magma', complete: false, priority: 0 },
  { id: 10, weight: 20, title: 'task 10', description: 'Tornado', complete: false, priority: 0 }
];