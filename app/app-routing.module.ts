import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TopTasksComponent }   from './top-tasks.component';
import { TasksComponent }      from './tasks.component';
import { TaskDetailComponent }  from './task-detail.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/top-tasks',
        pathMatch: 'full'
    },
    {
        path: 'tasks',
        component: TasksComponent
    },
    {
        path: 'top-tasks',
        component: TopTasksComponent
    },
    {
        path: 'detail/:id',
        component: TaskDetailComponent
    }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}