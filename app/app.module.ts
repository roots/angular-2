// Core
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

// Components
import { AppComponent } from './app.component';
import { TasksComponent }   from './tasks.component';
import { TaskDetailComponent } from './task-detail.component';
import { TopTasksComponent } from "./top-tasks.component";

// Services
import { TaskService } from './task.service';

// Routing
import { AppRoutingModule }     from './app-routing.module';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    TaskDetailComponent,
    TasksComponent,
    TopTasksComponent
  ],
  providers: [
    TaskService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }